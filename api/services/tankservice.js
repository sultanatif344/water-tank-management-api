const { User } = require('../../helpers/db');
const db = require('../../helpers/db');
const Tank = db.Tank;
var axios = require('axios');
module.exports = {
    createTank,
    getAllTanks,
    updateTank,
    delete: _deleteTank,
    toggleTank: toggleTank,
    sendNotification: sendTankStatusNotification
};


async function createTank(tankParam) {

    const user = new Tank(tankParam);
    // save user
    await user.save();
}

async function getAllTanks() {
    return await Tank.find();
}

async function updateTank(id, tankParam) {
    const tank = await Tank.findById(id);

    // copy userParam properties to user
    Object.assign(tank, tankParam);

    await tank.save();
}

async function _deleteTank(id) {
    await Tank.findByIdAndRemove(id);
}


function toggleTank(previousToggledTankActivationId, tankToToggleActivationId, toggleValue) {
    return new Promise(async resolve => {
        if (previousToggledTankActivationId != null) {
            const previousTank = await Tank.findById(previousToggledTankActivationId);
            console.log("tank here is deactivating");
            previousTank.activated = false;

            await previousTank.save();
        }
        console.log("tank updating...")
        const currentActivatedTank = await Tank.findById(tankToToggleActivationId);

        currentActivatedTank.activated = toggleValue;

        await currentActivatedTank.save();

        resolve({ "tankDeactivated": previousToggledTankActivationId, "tankActivated": tankToToggleActivationId, "success": true })
        // await Tank.
    })
}

function sendTankStatusNotification(body) {
    return new Promise((resolve, reject) => {
        console.log("here is mesage body", body);
        var data = JSON.stringify({
            "to": `${body.fcmToken}`,
            "collapse_key": "EaWLCt3N8dqQu3nburV6RyKxAu1U_M5DwY1Q8cwlZ0sG5lv07SFxdA97Hlr8QG69Q4c0G0AKZGbC3JE2zqneBXo6AhXNohZz9InhGtA0O_qAYZYsQN8D0BIQCHtfJiUz1RP6hT",
            "data": {
                "body": `${body.body}`,
                "title": `${body.title}`
            }
        });

        var config = {
            method: 'post',
            url: 'https://fcm.googleapis.com/fcm/send',
            headers: {
                'Authorization': 'key=AAAAw6HzvuQ:APA91bEaWLCt3N8dqQu3nburV6RyKxAu1U_M5DwY1Q8cwlZ0sG5lv07SFxdA97Hlr8QG69Q4c0G0AKZGbC3JE2zqneBXo6AhXNohZz9InhGtA0O_qAYZYsQN8D0BIQCHtfJiUz1RP6hT',
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                resolve(response.data);
            })
            .catch(function (error) {
                reject(error)
            });
    })
}

