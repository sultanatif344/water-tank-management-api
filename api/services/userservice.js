const config = require('../../nodemon.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../../helpers/db');
const User = db.User;
const Notification = db.NotificationHistory;
const Reviews = db.Reviews;
module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    addNotificationToken,
    resetUserPassword,
    saveNotificationHistory,
    deleteNotificationHistory,
    getAllNotifications,
    getAllReviews,
    createReview
};

async function authenticate({ username, password }) {
    const user = await User.findOne(username);
    console.log("user here", user)
    if (user && bcrypt.compareSync(password, user.password)) {
        const token = jwt.sign({ sub: user.id }, config.env.secret, { expiresIn: '7d' });
        return {
            ...user.toJSON(),
            token
        };
    }
}

async function getAll() {
    return await User.find();
}

async function getById(id) {
    return await User.findById(id);
}


async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.password = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

function update(id, userParam) {
    return new Promise(async resolve => {
        let user = await User.findById(id);
        console.log(userParam)
        if (userParam.username && userParam.username != user.username) {
            User.findByIdAndUpdate(id, { "username": userParam.username }, function (err, result) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            })
        }
        else {
            resolve(false)
        }
        if (userParam.email && userParam.email != user.email) {
            User.findByIdAndUpdate(id, { "email": userParam.email }, function (err, result) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            })
        }
        else {
            resolve(false)
        }
        if (userParam.userType && userParam.userType != user.userType) {
            User.findByIdAndUpdate(id, { "userType": userParam.userType }, function (err, result) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            })
        }
        else {
            resolve(false)
        }
        if (Object.keys(userParam.authorizations).length > 0) {
            User.findByIdAndUpdate(id, { "authorizations": userParam.authorizations }, function (err, result) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(result);
                }
            })
        } else {
            resolve(false)
        }
    })
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}

async function addNotificationToken(token, id) {
    return new Promise((resolve, reject) => {
        console.log("token here", token);
        console.log("id here", id);

        User.findByIdAndUpdate(id, { "notificationToken": token }, function (err, result) {
            if (err) {
                reject(err);
            }
            else {
                resolve(result);
            }
        })
    })
}

async function resetUserPassword(id, oldPassword, newPassword) {
    return new Promise(async (resolve, reject) => {
        let user = await User.findById(id);
        console.log("this is the old password", oldPassword)
        console.log("this is the new password", newPassword)
        console.log("compare", bcrypt.compareSync(oldPassword, user.password))
        if (user) {
            if (bcrypt.compareSync(oldPassword, user.password) == true) {
                console.log("running");
                await User.findByIdAndUpdate(id, { "password": bcrypt.hashSync(newPassword, 10) })
                resolve(true);
            }
            else {
                resolve(false)
            }
        }
        else {
            resolve(false);
        }
    })
}

async function saveNotificationHistory(notificationParam){
    const notification = new Notification(notificationParam);
    await notification.save();
}

async function deleteNotificationHistory(id) {
    await Notification.findByIdAndRemove(id);
}

async function getAllNotifications(){
    console.log("coming here")
    return await Notification.find();
}

async function getAllReviews(){
    return await Reviews.find();
}

async function createReview(reviewParam){
    console.log("this is my review", reviewParam)
    const review = new Reviews(reviewParam);
    await review.save();
}