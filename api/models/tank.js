const mongoose = require('mongoose');

const tankSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    width: { type: Number },
    height: { type: Number },
    length: { type: Number },
    diameter: { type: Number },
    tankShape: { type: String, required: true },
    activated: { type: Boolean, required: true }
})



module.exports = mongoose.model('Tank', tankSchema);