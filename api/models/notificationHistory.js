const mongoose = require('mongoose');

const notificationHistorySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    notificationType: { type: String, required: true },
    description: { type: String, required: true },
})



module.exports = mongoose.model('Notification', notificationHistorySchema);