const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    username: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    userType: { type: String, required: true },
    token: { type: String},
    authorizations: {
        waterLevel: { type: Boolean, required: true },
        waterStatus: { type: Boolean, required: true },
        addATank: { type: Boolean, required: true },
        tankList: { type: Boolean, required: true },
        motorStatus: { type: Boolean, required: true },
        adminPanel: { type: Boolean, required: true }
    },
    notificationToken: { type: String }
})



module.exports = mongoose.model('User', userSchema);