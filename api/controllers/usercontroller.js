const express = require('express');
const router = express.Router();
const userService = require('../services/userservice');
const mongoose = require('mongoose');


// routes
router.post('/authenticate', authenticate);
router.post('/register', register);
router.get('/getAll', getAll);
router.get('/current', getCurrent);
router.get('/getById/:id', getById);
router.put('/updateUser/:id', update);
router.delete('/deleteUser/:id', _delete);
router.put('/updateNotificationToken/:id', updateTokenForId);
router.post('/resetPassword/:id', resetPassword);
router.post('/saveNotificationHistory', saveHistory);
router.delete('/deleteNotificationHistory/:id', deleteHistory);
router.get('/getAllNotifications',sendNotificationList);
router.post('/AddUserReview',createUserReview)
router.get('/getAllReviews',getAllReviews)
module.exports = router;

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json({ user, success: true }) : res.status(400).json({ message: 'Username or password is incorrect', success: false }))
        .catch(err => next(err));
}

function register(req, res, next) {
    req.body["_id"] = mongoose.Types.ObjectId().toString();
    userService.create(req.body)
        .then(() => res.json({ message: 'User Created!', success: true }))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json({ users }))
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    userService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({ message: 'User Updated!', success: true }))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function updateTokenForId(req, res, next) {
    console.log("notification", req.body.notificationToken);
    console.log(req.params.id);
    userService.addNotificationToken(req.body.notificationToken, req.params.id)
        .then((result) => res.json({ result: result }))
        .catch(err => next(err))
}

function resetPassword(req, res, next) {
    userService.resetUserPassword(req.params.id, req.body.oldPassword, req.body.newPassword)
        .then((result) => res.json({ updated: result }))
        .catch(err => next(err))
}

function saveHistory(req, res, next){
    req.body["_id"] = mongoose.Types.ObjectId().toString();
    userService.saveNotificationHistory(req.body).then((result)=>{
        res.json({response:result})
    }).catch(err => next(err))
}

function deleteHistory(req, res, next){
    userService.deleteNotificationHistory(req.params.id).then((result)=>{
        res.json({response:result})
    }).catch(err => next(err))
}

function sendNotificationList(req, res, next){
    userService.getAllNotifications().then((result)=>{
        res.json({response:result})
    }).catch(err => next(err))
}

function createUserReview(req, res, next){
    console.log("this is my body", req.body);
    req.body["_id"] = mongoose.Types.ObjectId().toString();
    userService.createReview(req.body).then(()=>{
        res.json({status:200, message:"Review Created Successfully!", success:true})
    }).catch(err => next(res.json({status:400, message:"Unable To Save Review", success: false})))
}

function getAllReviews(req, res, next){
    userService.getAllReviews().then((result)=>{
        console.log(result);
        res.json({status:200, response:result, success:true})
    }).catch(err => res.json({status:400, response:[], success: false}))
}


























































// const express = require('express');
// const router = express.Router();
// const mongoose = require('mongoose');
// const User = require('../models/user');
// const bcrypt = require('bcrypt');
// router.post('/signup', (req, res, next) => {
//     console.log('coming here')
//     User.find({ email: req.body.email })
//         .exec()
//         .then(user => {
//             if (user.length >= 1) {
//                 return res.status(409).json({
//                     message: 'Mail exists'
//                 })
//             }
//             else {
//                 bcrypt.hash(req.body.password, 10, (err, hash) => {
//                     if (err) {
//                         return res.status(500).json({
//                             error: err
//                         });
//                     }
//                     else {
//                         console.log('completed')
//                         const user = new User({
//                             _id: new mongoose.Types.ObjectId(),
//                             username: req.body.userName,
//                             email: req.body.email,
//                             passowrd: hash,
//                             userType: req.body.userType,
//                             authorizations: {
//                                 waterLevel: req.body.waterLevel,
//                                 waterStatus: req.body.waterStatus,
//                                 addATank: req.body.addATank,
//                                 tankList: req.body.tankList,
//                                 motorStatus: req.body.motorStatus,
//                                 adminPanel: req.body.adminPanel
//                             }
//                         })
//                         User.create(user).then(result => {
//                             res.status(200).json({
//                                 message: 'User Created'
//                             })
//                         });
//                     }
//                 })
//             }
//         })
//         .catch(err => console.log(err))
// });

// module.exports = router;